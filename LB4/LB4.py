import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.linear_model import Ridge


header = ['Sex', 'Length', 'Diameter', 'Height', 'Whole weight', 'Shucked weight', 'Viscera weight', 'Shell weight',
          'Rings']
data = pd.read_csv('./abaloneWithRemoves.data', names=header)

dummies = pd.get_dummies(data['Sex'])
data = pd.concat([data, dummies], axis=1)
data = data.drop('Sex', axis=1)

x = data.drop('Rings', axis=1)
y = data['Rings']

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.4)

standardScale = StandardScaler()
standardScale.fit(x_train)

standardScale.transform(x_train)
standardScale.transform(x_test)


pca = PCA(n_components=6)
pca.fit(x_train)

x_train = pca.transform(x_train)
x_test = pca.transform(x_test)

model = Ridge()
model.fit(x_train, y_train)

print(model.score(x_test, y_test))

per_var = np.round(pca.explained_variance_ratio_*100, decimals=1)
labels = ['PC' + str(x) for x in range(1, len(per_var)+1)]
plt.bar(x=range(1, len(per_var)+1), height=per_var, tick_label = labels)
plt.ylabel('Percentage of Explained Variance')
plt.xlabel('Principal Component')
plt.title('Scree Plot')
plt.show()


