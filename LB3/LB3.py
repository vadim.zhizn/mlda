import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

from sklearn.linear_model import Ridge
from sklearn.metrics import r2_score


header = ['Sex', 'Length', 'Diameter', 'Height', 'Whole weight', 'Shucked weight', 'Viscera weight', 'Shell weight',
              'Rings']
data = pd.read_csv('./abaloneWithRemoves.data', names=header)

dummies = pd.get_dummies(data['Sex'])
data = pd.concat([data, dummies], axis=1)
data = data.drop('Sex', axis=1)

x = data.drop('Rings', axis=1)
y = data['Rings']

standardScale = StandardScaler()
standardScale.fit(x)

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.4)

model = Ridge()
model.fit(x_train, y_train)
print(r2_score(y_test, model.predict(x_test)))

predictors = [x for x in data.columns if x not in ['Rings']]
fi = pd.Series(model.coef_, predictors).sort_values(ascending=False)

plt.figure(figsize=(20, 4))
fi.plot(kind='bar', title='Fi')
plt.tight_layout()
plt.show()