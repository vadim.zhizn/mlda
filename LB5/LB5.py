import pandas as pd
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeRegressor
from xgboost import XGBRegressor

header = ['Sex', 'Length', 'Diameter', 'Height', 'Whole weight', 'Shucked weight', 'Viscera weight', 'Shell weight',
          'Rings']
data = pd.read_csv('./abaloneWithRemoves.data', names=header)

dummies = pd.get_dummies(data['Sex'])
data = pd.concat([data, dummies], axis=1)
data = data.drop('Sex', axis=1)

x = data.drop('Rings', axis=1)
y = data['Rings']

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.4)

ss = StandardScaler()
ss.fit(x_train)

ss.transform(x_train)
ss.transform(x_test)


age_tree_3 = DecisionTreeRegressor(max_depth=3).fit(x_train, y_train)
age_tree_4 = DecisionTreeRegressor(max_depth=4).fit(x_train, y_train)
age_tree_5 = DecisionTreeRegressor(max_depth=5).fit(x_train, y_train)
age_tree_6 = DecisionTreeRegressor(max_depth=6).fit(x_train, y_train)

print("Score of DecisionTreeRegressor with max depth = 3: "+ str(age_tree_3.score(x_test, y_test)))
print("Score of DecisionTreeRegressor with max depth = 4: "+ str(age_tree_4.score(x_test, y_test)))
print("Score of DecisionTreeRegressor with max depth = 5: "+ str(age_tree_5.score(x_test, y_test)))
print("Score of DecisionTreeRegressor with max depth = 6: "+ str(age_tree_6.score(x_test, y_test)))

rf = RandomForestRegressor().fit(x_train, y_train)
print("Score of RandomForestRegressor: " + str(rf.score(x_test, y_test)))

xg = XGBRegressor(verbosity=0).fit(x_train, y_train)
print("Score of XGBoostRegressor: ", str(xg.score(x_test, y_test)))

ada = AdaBoostRegressor().fit(x_train, y_train)
print("Score of AdaBoostRegressor: ", str(ada.score(x_test, y_test)))
